package arrayexercise;

//Java program to find index of 
//an element in N elements 
import java.util.Arrays; 


public class JavaArrayExercise {
	
	  public static boolean contains(int[] arr, int item) {
	      for (int n : arr) {
	         if (item == n) {
	            return true;
	         }
	      }
	      return false;
	   }
	  

	    // Function to find the index of an element 
	    public static int findIndex(int arr[], int t) 
	    { 
	  
	        int index = Arrays.binarySearch(arr, t); 
	        return (index < 0) ? -1 : index; 
	    } 
	    
	    // Method for getting the maximum value
	    public static int getMax(int[] inputArray){ 
	      int maxValue = inputArray[0]; 
	      for(int i=1;i < inputArray.length;i++){ 
	        if(inputArray[i] > maxValue){ 
	           maxValue = inputArray[i]; 
	        } 
	      } 
	      return maxValue; 
	    }
	   
	    // Method for getting the minimum value
	    public static int getMin(int[] inputArray){ 
	      int minValue = inputArray[0]; 
	      for(int i=1;i<inputArray.length;i++){ 
	        if(inputArray[i] < minValue){ 
	          minValue = inputArray[i]; 
	        } 
	      } 
	      return minValue; 
	    }
	    
	    public static int removeDuplicateElements(int arr[], int n){  
	        if (n==0 || n==1){  
	            return n;  
	        }  
	        int[] temp = new int[n];  
	        int j = 0;  
	        for (int i=0; i<n-1; i++){  
	            if (arr[i] != arr[i+1]){  
	                temp[j++] = arr[i];  
	            }  
	         }  
	        temp[j++] = arr[n-1];     
	        // Changing original array  
	        for (int i=0; i<j; i++){  
	            arr[i] = temp[i];  
	        }  
	        return j;  
	    } 
	    
	    
	    public static int getSecondLargest(int[] a, int total){  
	    	int temp;  
	    	for (int i = 0; i < total; i++)   
	    	        {  
	    	            for (int j = i + 1; j < total; j++)   
	    	            {  
	    	                if (a[i] > a[j])   
	    	                {  
	    	                    temp = a[i];  
	    	                    a[i] = a[j];  
	    	                    a[j] = temp;  
	    	                }  
	    	            }  
	    	        }  
	    	       return a[total-2];  
	    	}  

	public static void main(String[] args) {

		 
	  int[] numbers = new int[]{10,20,30,40,50,60,70,80,90,100};
	
	   	
	   	   
	       //calculate sum of all array elements
	       int sum = 0;
	       for(int i=0; i < numbers.length ; i++)
	        sum = sum + numbers[i];
	       
	       		System.out.println("sum value is: "+ sum);
	       
	       // to find the array length
	       		System.out.println("Array length is: "+ numbers.length);
	       
	       //calculate average value
	        double average = sum / numbers.length;
		
	        	System.out.println("Average value of the array elements is : " + average); 
	        
	       // To test if an array contains a specific value
	        	System.out.println("This array contains a specific value:"+contains(numbers, 60));
	        	System.out.println("This array contains a specific value:"+contains(numbers, 110));
	         
	    
	        // To find the index of an array element
	            System.out.println("Index position of 60 is: " + findIndex(numbers, 50)); 
	            System.out.println("Index position of 110 is: " + findIndex(numbers, 110));
	            
	        // To remove a specific element from the array
	            System.out.println("Original Array : "+Arrays.toString(numbers)); 
	            
	            // Remove the second element  of the array
	            	int removeIndex = 1;

	            	for(int i = removeIndex; i <numbers.length -1; i++){
	            		numbers[i] = numbers[i + 1];
	               }
	         
	            System.out.println("After removing the second element: "+Arrays.toString(numbers));
	            
	        // To copy an array by iterating the array
	            int[] new_array = new int[10];     
	            
	            for(int i=0; i < numbers.length; i++) {
	             new_array[i] = numbers[i];
	            }
	            System.out.println("New Array: "+Arrays.toString(new_array));
	            
	        // To insert an element (specific position) into an array
			            int Index_position = 2;
			            int newValue    = 40;
			            
			            for(int i=numbers.length-1; i > Index_position; i--){
			                numbers[i] = numbers[i-1];
			            }
			            numbers[Index_position] = newValue;
	               System.out.println("After insert element into Array: "+Arrays.toString(numbers));
	               
	       // To find the maximum and minimum value of an array
	               // Calling maximum value
	               int max = getMax(numbers);
	               System.out.println("Maximum Value is: "+max);
	            
	               // Calling minimum value
	               int min = getMin(numbers);
	               System.out.println("Minimum Value is: "+min);
	               
	      // To reverse an array of integer values
	               for(int i = 0; i < numbers.length / 2; i++){
	                 int temp = numbers[i];
	                 numbers[i] = numbers[numbers.length - i - 1];
	                 numbers[numbers.length - i - 1] = temp;
	               }
	                 System.out.println("Reverse array : "+Arrays.toString(numbers));
	                 
	      // To find the duplicate values of an array of integer values
	                 for (int i = 0; i < numbers.length-1; i++) {
	                     for (int j = i+1; j < numbers.length; j++) {
	                    	 if ((numbers[i] == numbers[j]) && (i != j)) {
	                             System.out.println("Duplicate Element : "+numbers[j]);
	                         }
	                     }
	                 }
	                 
	      // To find common elements between two arrays
	                 int[] array1 = {1, 2, 5, 5, 8, 9, 7, 10};
	                 int[] array2 = {1, 0, 6, 15, 6, 4, 7, 0};
	            
	                  System.out.println("Array1 : "+Arrays.toString(array1));
	                  System.out.println("Array2 : "+Arrays.toString(array2));
	            
	                 
	                   for (int i = 0; i < array1.length; i++) {
	                       for (int j = 0; j < array2.length; j++) {
	                           if(array1[i] == (array2[j])) {
	                            System.out.println("Common element is : "+(array1[i]));
	                            }
	                       }
	                   }
	           
	       // To remove duplicate elements from an array
	                   int arr[] = {10,20,20,30,30,40,50,50};  
	                   int length = arr.length;  
	                   length = removeDuplicateElements(arr, length);  
	                   //printing array elements  
	                   for (int i=0; i<length; i++)  
	                      System.out.print(arr[i]+" ");  
	                   
	      // To find the second largest elements from an array
	                   int a[]={1,2,5,6,3,2};  
	                   int b[]={44,66,99,77,33,22,55};  
	                   System.out.println("\n"+"Second Largest: "+getSecondLargest(a,6));  
	                   System.out.println("Second Largest: "+getSecondLargest(b,7)); 
	   }

}
